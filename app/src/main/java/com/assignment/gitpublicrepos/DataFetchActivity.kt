package com.assignment.gitpublicrepos

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.Window.FEATURE_NO_TITLE
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import com.assignment.gitpublicrepos.adapter.RepositoryAdapter
import com.assignment.gitpublicrepos.contracts.RepositoryContracts
import com.assignment.gitpublicrepos.model.RepoResponseModel
import com.assignment.gitpublicrepos.presenter.RepositoryPresenter
import kotlinx.android.synthetic.main.activity_data_fetch.*

class DataFetchActivity : AppCompatActivity(),RepositoryContracts.Callbacks {
    lateinit var presenter: RepositoryPresenter
    lateinit var mLayoutManager: LinearLayoutManager
    lateinit var adapter: RepositoryAdapter
    lateinit var mProgressBar: ProgressBar

    var mDataList: MutableList<RepoResponseModel>  = mutableListOf()
    var loading = true
    override fun showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
      mProgressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        mProgressBar.visibility = View.GONE
    }

    override fun onSuccess(modelList: List<RepoResponseModel>?) {
        mDataList.addAll(modelList as MutableList<RepoResponseModel>)
        adapter.setData(mDataList)
        loading = true

    }

    override fun onFailure(error: String) {
        Toast.makeText(this@DataFetchActivity, error, Toast.LENGTH_SHORT).show()
    }

    lateinit  var mRepoRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(FEATURE_NO_TITLE)
        setContentView(R.layout.activity_data_fetch)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        mProgressBar = progressbar
        mDataList.clear()
        mRepoRecyclerView = list_repositories
        adapter = RepositoryAdapter(this@DataFetchActivity,mDataList)
        loadMoreData()
        mLayoutManager = LinearLayoutManager(this@DataFetchActivity)
        mRepoRecyclerView.layoutManager = mLayoutManager
        mRepoRecyclerView.adapter = adapter

        val map = HashMap<String,String>()
        map.put("since","0")
        map.put("rel","next")

        presenter = RepositoryPresenter(this@DataFetchActivity,this)
        presenter.loadData(map)




    }

    fun loadMoreData(){

        var pastVisiblesItems: Int
        var visibleItemCount: Int
        var totalItemCount: Int

        mRepoRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (dy > 0)
                //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount()
                    totalItemCount = mLayoutManager.getItemCount()
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()

                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            //Log.v("...", "Last Item Wow !")

                            val map = HashMap<String,String>()
                            map.put("since",mDataList.get(totalItemCount-1).id.toString());
                            map.put("rel","next")
                            presenter.loadData(map)

                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        })
    }



}
