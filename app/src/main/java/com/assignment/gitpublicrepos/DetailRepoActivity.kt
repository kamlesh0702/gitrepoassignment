package com.assignment.gitpublicrepos

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import com.assignment.gitpublicrepos.adapter.BranchAdapter
import com.assignment.gitpublicrepos.adapter.ContainerAdapter
import com.assignment.gitpublicrepos.adapter.LanguageAdapter
import com.assignment.gitpublicrepos.model.BranchResponse
import com.assignment.gitpublicrepos.model.Contributor
import com.assignment.gitpublicrepos.model.RepoResponseModel
import com.assignment.gitpublicrepos.networking.APIClient
import com.assignment.gitpublicrepos.networking.GitHubService
import kotlinx.android.synthetic.main.activity_detail_repo.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailRepoActivity : AppCompatActivity() {
    var isBranchExpanded= false
    var isContributorExpanded = false
    var isLanguageExpanden = false

     var contributorList: List<Contributor> = emptyList()
    var languageList: String = ""
    var branchList: List<BranchResponse> = emptyList()
    lateinit var  gitHubService: GitHubService
    lateinit var repositoryModel: RepoResponseModel
    lateinit var mProgressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_detail_repo)
        mProgressBar = progressbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        repositoryModel = intent.getSerializableExtra(resources.getString(R.string.bundle_repo)) as RepoResponseModel
        title = repositoryModel.name
        gitHubService = APIClient().getClient().create(GitHubService::class.java)



    }

    fun showProgress(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        mProgressBar.visibility = View.VISIBLE
    }

    fun hideProgress(){
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        mProgressBar.visibility = View.GONE
    }

    fun contributorClicked(view: View){

        if(contributorList.isEmpty()){
            val conributorCall =  gitHubService.getContributors(repositoryModel.contributorsUrl)
            showProgress()
            conributorCall.enqueue(object : Callback<List<Contributor>>{
                override fun onResponse(call: Call<List<Contributor>>?, response: Response<List<Contributor>>?) {
                    hideProgress()
                    contributorList = response?.body()?: emptyList()
                    val adapter = ContainerAdapter(this@DetailRepoActivity,contributorList)
                    list_container.layoutManager = LinearLayoutManager(this@DetailRepoActivity)
                    list_container.adapter = adapter


                }

                override fun onFailure(call: Call<List<Contributor>>?, t: Throwable?) {
                    hideProgress()
                    Toast.makeText(this@DetailRepoActivity,"Something went wrong",Toast.LENGTH_SHORT).show()
                }
            })
        }
        if(isContributorExpanded){
            list_container.visibility= View.GONE
            isContributorExpanded = false
        }else{
            list_container.visibility= View.VISIBLE
            isContributorExpanded = true
        }
    }

    fun branchClicked(view: View){

        val branchUrl = repositoryModel.branchesUrl.replace("{/branch}","")
        if(branchList.isEmpty()) {
            val branchCall = gitHubService.getBranches(branchUrl)
            showProgress()
            branchCall.enqueue(object : Callback<List<BranchResponse>> {
                override fun onResponse(call: Call<List<BranchResponse>>?, response: Response<List<BranchResponse>>?) {
                    hideProgress()
                    branchList = response?.body() ?: emptyList()
                    val adapter = BranchAdapter(this@DetailRepoActivity, branchList)
                    list_branches.layoutManager = LinearLayoutManager(this@DetailRepoActivity)
                    list_branches.adapter = adapter


                }

                override fun onFailure(call: Call<List<BranchResponse>>?, t: Throwable?) {
                    hideProgress()
                    Toast.makeText(this@DetailRepoActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            })
        }
        if(isBranchExpanded){
            list_branches.visibility= View.GONE
            isBranchExpanded = false
        }else{
            list_branches.visibility= View.VISIBLE
            isBranchExpanded = true
        }
    }

    fun languageClicked(view: View){


        val scalarGitHubService = APIClient().getScalarClient().create(GitHubService::class.java)
        if(TextUtils.isEmpty(languageList)) {
            val languageCall= scalarGitHubService.getLanguages(repositoryModel.languagesUrl)
            showProgress()
            languageCall.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>?, response: Response<String>?) {
                    hideProgress()
                    languageList = response?.body() ?:""



                    val jsonObject = JSONObject(languageList)
                    val languageStringList = jsonObject.keys()

                    var languageSource = mutableListOf<String>()

                    for(str in languageStringList){
                        languageSource.add(str)
                    }

                    val adapter = LanguageAdapter(this@DetailRepoActivity, languageSource)
                    list_language.layoutManager = LinearLayoutManager(this@DetailRepoActivity)
                    list_language.adapter = adapter


                }

                override fun onFailure(call: Call<String>?, t: Throwable?) {
                    hideProgress()
                    Toast.makeText(this@DetailRepoActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            })
        }
        if(isLanguageExpanden){
            list_language.visibility= View.GONE
            isLanguageExpanden = false
        }else{
            list_language.visibility= View.VISIBLE
            isLanguageExpanden = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home){

           finish()

        }
        return super.onOptionsItemSelected(item)

    }
}
