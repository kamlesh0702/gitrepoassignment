package com.assignment.gitpublicrepos.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.assignment.gitpublicrepos.R
import com.assignment.gitpublicrepos.model.BranchResponse

class BranchAdapter(val context: Context, val branchList: List<BranchResponse>?) : RecyclerView.Adapter<BranchAdapter.BranchViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BranchViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_branch,parent,false)
        return  BranchViewHolder(view)
    }

    override fun getItemCount(): Int {
       return branchList?.size?:0
    }

    override fun onBindViewHolder(holder: BranchViewHolder, position: Int) {
        holder.bind(branchList?.get(position))
    }

    class BranchViewHolder(private val view: View) : RecyclerView.ViewHolder(view){
        val mTxtName: TextView
        init {
            mTxtName = view.findViewById(R.id.txt_row_branch)
        }

        fun bind(model: BranchResponse?) {
            mTxtName.text = model?.name?:""
        }
    }
}