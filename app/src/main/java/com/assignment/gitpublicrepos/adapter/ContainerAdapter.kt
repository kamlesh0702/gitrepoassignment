package com.assignment.gitpublicrepos.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.assignment.gitpublicrepos.R
import com.assignment.gitpublicrepos.model.Contributor

class ContainerAdapter(val context: Context, val contributorList: List<Contributor>?) : RecyclerView.Adapter<ContainerAdapter.ContainerViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContainerViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.row_container,parent,false)
        return ContainerViewHolder(view)
    }

    override fun getItemCount(): Int {
      return contributorList?.size?:0
    }

    override fun onBindViewHolder(holder: ContainerViewHolder, position: Int) {
        val model = contributorList?.get(position)
        holder.bind(model)
    }

    class ContainerViewHolder(private val view: View) : RecyclerView.ViewHolder(view){
        val mTxtName: TextView
        init {
            mTxtName = view.findViewById(R.id.txt_row_container)
        }

        fun bind(model: Contributor?) {
            mTxtName.text = model?.login?:""
        }
    }
}