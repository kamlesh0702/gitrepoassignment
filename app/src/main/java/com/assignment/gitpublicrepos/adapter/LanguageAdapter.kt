package com.assignment.gitpublicrepos.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.assignment.gitpublicrepos.R

class  LanguageAdapter(val context: Context,val langages: MutableList<String>) : RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_language,parent,false)
        return LanguageViewHolder(view)
    }

    override fun getItemCount(): Int {
       return langages?.size?:0
    }

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
       holder.bind(langages?.get(position))
    }


    class LanguageViewHolder(private val view: View) : RecyclerView.ViewHolder(view){
        val mTxtName: TextView
        init {
            mTxtName = view.findViewById(R.id.txt_row_language)
        }

        fun bind(value: String?) {
            mTxtName.text = value?:""
        }
    }
}