package com.assignment.gitpublicrepos.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.assignment.gitpublicrepos.DetailRepoActivity
import com.assignment.gitpublicrepos.R
import com.assignment.gitpublicrepos.model.RepoResponseModel

class RepositoryAdapter(val context: Context,var responseList: List<RepoResponseModel>?) : RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder>(){



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val inflater = LayoutInflater.from(context)
        var binding = inflater.inflate(R.layout.row_repository,parent,false)
        return RepositoryViewHolder(binding)
    }

    override fun getItemCount(): Int {
       return responseList?.size?:0
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        var response = responseList?.get(position)
        holder.bind(response )
    }


    /*class RepositoryViewHolder(private val view: ViewDataBinding) : RecyclerView.ViewHolder(viewDataBinding.root){

        fun bind(model: RepoResponseModel?) {
            viewDataBinding.setVariable(BR.response,model)
            viewDataBinding.executePendingBindings()
        }
    }*/

    inner class RepositoryViewHolder(private val view: View) : RecyclerView.ViewHolder(view){
    val mTxtName: TextView
        init {
            mTxtName = view.findViewById(R.id.txt_row_repository)
    }

        fun bind(model: RepoResponseModel?) {
            mTxtName.text = model?.name?:""

            view.setOnClickListener(View.OnClickListener {

                val intent = Intent(context,DetailRepoActivity::class.java)
                intent.putExtra(context.resources.getString(R.string.bundle_repo),model)
                context.startActivity(intent)
            })
        }
    }

    fun setData(tempList:  List<RepoResponseModel>?){
        responseList = tempList
        notifyDataSetChanged()
    }
}