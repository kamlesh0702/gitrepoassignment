package com.assignment.gitpublicrepos.contracts

import com.assignment.gitpublicrepos.model.RepoResponseModel
import java.util.HashMap

class RepositoryContracts{
    interface Actions {
        fun loadData(map: HashMap<String, String>)
    }

    interface Callbacks {
        fun showProgress()
        fun hideProgress()
        fun onSuccess(modelList: List<RepoResponseModel>?)
        fun onFailure(error: String)
    }
}