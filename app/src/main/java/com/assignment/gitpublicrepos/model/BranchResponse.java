package com.assignment.gitpublicrepos.model;

import java.io.Serializable;

public class BranchResponse implements Serializable {
    private String name;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    private Commit commit;

    public Commit getCommit() { return this.commit; }

    public void setCommit(Commit commit) { this.commit = commit; }

    public class Commit
    {
        private String sha;

        public String getSha() { return this.sha; }

        public void setSha(String sha) { this.sha = sha; }

        private String url;

        public String getUrl() { return this.url; }

        public void setUrl(String url) { this.url = url; }
    }




}
