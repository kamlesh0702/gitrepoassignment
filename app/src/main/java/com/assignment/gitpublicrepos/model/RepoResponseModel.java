package com.assignment.gitpublicrepos.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RepoResponseModel implements Serializable
{
    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    private String node_id;

    public String getNodeId() { return this.node_id; }

    public void setNodeId(String node_id) { this.node_id = node_id; }

    private String name;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    private String full_name;

    public String getFullName() { return this.full_name; }

    public void setFullName(String full_name) { this.full_name = full_name; }

    private Owner owner;

    public Owner getOwner() { return this.owner; }

    public void setOwner(Owner owner) { this.owner = owner; }

    @SerializedName("private")
    private boolean isPrivate;

    public boolean getPrivate() { return this.isPrivate; }

    public void setPrivate(boolean isPrivate) { this.isPrivate = isPrivate; }

    private String html_url;

    public String getHtmlUrl() { return this.html_url; }

    public void setHtmlUrl(String html_url) { this.html_url = html_url; }

    private String description;

    public String getDescription() { return this.description; }

    public void setDescription(String description) { this.description = description; }

    private boolean fork;

    public boolean getFork() { return this.fork; }

    public void setFork(boolean fork) { this.fork = fork; }

    private String url;

    public String
    getUrl() { return this.url; }

    public void setUrl(String url) { this.url = url; }

    private String archive_url;

    public String getArchiveUrl() { return this.archive_url; }

    public void setArchiveUrl(String archive_url) { this.archive_url = archive_url; }

    private String assignees_url;

    public String getAssigneesUrl() { return this.assignees_url; }

    public void setAssigneesUrl(String assignees_url) { this.assignees_url = assignees_url; }

    private String blobs_url;

    public String getBlobsUrl() { return this.blobs_url; }

    public void setBlobsUrl(String blobs_url) { this.blobs_url = blobs_url; }

    private String branches_url;

    public String getBranchesUrl() { return this.branches_url; }

    public void setBranchesUrl(String branches_url) { this.branches_url = branches_url; }

    private String collaborators_url;

    public String getCollaboratorsUrl() { return this.collaborators_url; }

    public void setCollaboratorsUrl(String collaborators_url) { this.collaborators_url = collaborators_url; }

    private String comments_url;

    public String getCommentsUrl() { return this.comments_url; }

    public void setCommentsUrl(String comments_url) { this.comments_url = comments_url; }

    private String commits_url;

    public String getCommitsUrl() { return this.commits_url; }

    public void setCommitsUrl(String commits_url) { this.commits_url = commits_url; }

    private String compare_url;

    public String getCompareUrl() { return this.compare_url; }

    public void setCompareUrl(String compare_url) { this.compare_url = compare_url; }

    private String contents_url;

    public String getContentsUrl() { return this.contents_url; }

    public void setContentsUrl(String contents_url) { this.contents_url = contents_url; }

    private String contributors_url;

    public String getContributorsUrl() { return this.contributors_url; }

    public void setContributorsUrl(String contributors_url) { this.contributors_url = contributors_url; }

    private String deployments_url;

    public String getDeploymentsUrl() { return this.deployments_url; }

    public void setDeploymentsUrl(String deployments_url) { this.deployments_url = deployments_url; }

    private String downloads_url;

    public String getDownloadsUrl() { return this.downloads_url; }

    public void setDownloadsUrl(String downloads_url) { this.downloads_url = downloads_url; }

    private String events_url;

    public String getEventsUrl() { return this.events_url; }

    public void setEventsUrl(String events_url) { this.events_url = events_url; }

    private String forks_url;

    public String getForksUrl() { return this.forks_url; }

    public void setForksUrl(String forks_url) { this.forks_url = forks_url; }

    private String git_commits_url;

    public String getGitCommitsUrl() { return this.git_commits_url; }

    public void setGitCommitsUrl(String git_commits_url) { this.git_commits_url = git_commits_url; }

    private String git_refs_url;

    public String getGitRefsUrl() { return this.git_refs_url; }

    public void setGitRefsUrl(String git_refs_url) { this.git_refs_url = git_refs_url; }

    private String git_tags_url;

    public String getGitTagsUrl() { return this.git_tags_url; }

    public void setGitTagsUrl(String git_tags_url) { this.git_tags_url = git_tags_url; }

    private String git_url;

    public String getGitUrl() { return this.git_url; }

    public void setGitUrl(String git_url) { this.git_url = git_url; }

    private String issue_comment_url;

    public String getIssueCommentUrl() { return this.issue_comment_url; }

    public void setIssueCommentUrl(String issue_comment_url) { this.issue_comment_url = issue_comment_url; }

    private String issue_events_url;

    public String getIssueEventsUrl() { return this.issue_events_url; }

    public void setIssueEventsUrl(String issue_events_url) { this.issue_events_url = issue_events_url; }

    private String issues_url;

    public String getIssuesUrl() { return this.issues_url; }

    public void setIssuesUrl(String issues_url) { this.issues_url = issues_url; }

    private String keys_url;

    public String getKeysUrl() { return this.keys_url; }

    public void setKeysUrl(String keys_url) { this.keys_url = keys_url; }

    private String labels_url;

    public String getLabelsUrl() { return this.labels_url; }

    public void setLabelsUrl(String labels_url) { this.labels_url = labels_url; }

    private String languages_url;

    public String getLanguagesUrl() { return this.languages_url; }

    public void setLanguagesUrl(String languages_url) { this.languages_url = languages_url; }

    private String merges_url;

    public String getMergesUrl() { return this.merges_url; }

    public void setMergesUrl(String merges_url) { this.merges_url = merges_url; }

    private String milestones_url;

    public String getMilestonesUrl() { return this.milestones_url; }

    public void setMilestonesUrl(String milestones_url) { this.milestones_url = milestones_url; }

    private String notifications_url;

    public String getNotificationsUrl() { return this.notifications_url; }

    public void setNotificationsUrl(String notifications_url) { this.notifications_url = notifications_url; }

    private String pulls_url;

    public String getPullsUrl() { return this.pulls_url; }

    public void setPullsUrl(String pulls_url) { this.pulls_url = pulls_url; }

    private String releases_url;

    public String getReleasesUrl() { return this.releases_url; }

    public void setReleasesUrl(String releases_url) { this.releases_url = releases_url; }

    private String ssh_url;

    public String getSshUrl() { return this.ssh_url; }

    public void setSshUrl(String ssh_url) { this.ssh_url = ssh_url; }

    private String stargazers_url;

    public String getStargazersUrl() { return this.stargazers_url; }

    public void setStargazersUrl(String stargazers_url) { this.stargazers_url = stargazers_url; }

    private String statuses_url;

    public String getStatusesUrl() { return this.statuses_url; }

    public void setStatusesUrl(String statuses_url) { this.statuses_url = statuses_url; }

    private String subscribers_url;

    public String getSubscribersUrl() { return this.subscribers_url; }

    public void setSubscribersUrl(String subscribers_url) { this.subscribers_url = subscribers_url; }

    private String subscription_url;

    public String getSubscriptionUrl() { return this.subscription_url; }

    public void setSubscriptionUrl(String subscription_url) { this.subscription_url = subscription_url; }

    private String tags_url;

    public String getTagsUrl() { return this.tags_url; }

    public void setTagsUrl(String tags_url) { this.tags_url = tags_url; }

    private String teams_url;

    public String getTeamsUrl() { return this.teams_url; }

    public void setTeamsUrl(String teams_url) { this.teams_url = teams_url; }

    private String trees_url;

    public String getTreesUrl() { return this.trees_url; }

    public void setTreesUrl(String trees_url) { this.trees_url = trees_url; }
}
