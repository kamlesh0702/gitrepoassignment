package com.assignment.gitpublicrepos.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class APIClient{

        lateinit var retrofit: Retrofit


    fun  getClient(): Retrofit{
            retrofit = Retrofit.Builder().baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create()).build()
        return retrofit
    }

    fun  getScalarClient(): Retrofit{
        retrofit = Retrofit.Builder().baseUrl("https://api.github.com/")
                .addConverterFactory(ScalarsConverterFactory.create()).build()
        return retrofit
    }
}