package com.assignment.gitpublicrepos.networking

import com.assignment.gitpublicrepos.model.BranchResponse
import com.assignment.gitpublicrepos.model.Contributor
import com.assignment.gitpublicrepos.model.RepoResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.Url

public interface GitHubService {


    @GET("repositories?")
        fun getPublicRepositories(@QueryMap options:  Map<String, String>): Call<List<RepoResponseModel>>

    @GET
        fun getContributors(@Url url: String): Call<List<Contributor>>

    @GET
        fun getBranches(@Url url: String): Call<List<BranchResponse>>

    @GET
        fun getLanguages(@Url url: String): Call<String>
}