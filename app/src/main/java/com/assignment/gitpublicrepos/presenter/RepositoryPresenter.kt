package com.assignment.gitpublicrepos.presenter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.assignment.gitpublicrepos.adapter.RepositoryAdapter
import com.assignment.gitpublicrepos.contracts.RepositoryContracts
import com.assignment.gitpublicrepos.model.RepoResponseModel
import com.assignment.gitpublicrepos.networking.APIClient
import com.assignment.gitpublicrepos.networking.GitHubService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class RepositoryPresenter(val context: Activity, val view: RepositoryContracts.Callbacks) : RepositoryContracts.Actions{
    override fun loadData(map: HashMap<String, String>) {
        val apiClient = APIClient().getClient().create(GitHubService::class.java)
        val call = apiClient.getPublicRepositories(map)
        context.runOnUiThread(Runnable {
            view.showProgress()
        })
        call.enqueue(ResponseObserver())
    }




    inner class ResponseObserver : Callback<List<RepoResponseModel>> {
        override fun onFailure(call: Call<List<RepoResponseModel>>?, t: Throwable?) {
            context.runOnUiThread(Runnable {
                view.hideProgress()
                view.onFailure("Something went wrong. Please try again later.")
            })

        }

        override fun onResponse(call: Call<List<RepoResponseModel>>?, response: Response<List<RepoResponseModel>>?) {
            var responseList =  response?.body()
            context.runOnUiThread(Runnable {
                view.hideProgress()
                view.onSuccess(responseList)
            })



        }

    }
}